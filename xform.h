#ifndef VSF_XFORM_H
#define VSF_XFORM_H

struct vsf_session;

/* xform_mangle_username()
 * PURPOSE
 * Called before user authentication to transform the username as configured.
 * This function will mangle the session's username as configured by
 * the various user_transform_* options.
 * PARAMETERS
 * p_sess         - the current session object
 */
void xform_mangle_username(struct vsf_session* p_sess);

/* xform_unmangle_username()
 * PURPOSE
 * Called after a failed authentication attempt to restore the username.
 * This function will restore the session's username as it was before
 * the xform_mangle_username() invocation.
 * PARAMETERS
 * p_sess         - the current session object
 */
void xform_unmangle_username(struct vsf_session* p_sess);

/* xform_finish()
 * PURPOSE
 * Called after user authentication to clean up after mangling.
 * This function will free the temporary data storage used for
 * the session username's mangling process.
 * PARAMETERS
 * p_sess         - the current session object
 */
void xform_finish(struct vsf_session* p_sess);

#endif /* VSF_XFORM_H */

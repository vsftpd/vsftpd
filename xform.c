/*
 * Part of Very Secure FTPd
 * Licence: GPL v2
 * Author: Peter Pentchev
 * xform.c
 *
 * Code to transform the client-supplied username prior to authentication
 * and actual Unix account operations.
 */

#include "session.h"
#include "tunables.h"
#include "xform.h"

void
xform_mangle_username(struct vsf_session* p_sess)
{
  str_copy(&p_sess->user_unmangled_str, &p_sess->user_str);
  if (tunable_xform_add_prefix[0] == '\0' &&
      tunable_xform_add_suffix[0] == '\0')
    return;
  str_empty(&p_sess->user_str);
  str_append_text(&p_sess->user_str, tunable_xform_add_prefix);
  str_append_str(&p_sess->user_str, &p_sess->user_unmangled_str);
  str_append_text(&p_sess->user_str, tunable_xform_add_suffix);
}

void
xform_unmangle_username(struct vsf_session* p_sess)
{
  str_copy(&p_sess->user_str, &p_sess->user_unmangled_str);
  str_empty(&p_sess->user_unmangled_str);
}

void
xform_finish(struct vsf_session* p_sess)
{
  str_free(&p_sess->user_unmangled_str);
}
